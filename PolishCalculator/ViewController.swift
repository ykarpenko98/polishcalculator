//
//  ViewController.swift
//  PolishCalculator
//
//  Created by Юрий on 9/26/18.
//  Copyright © 2018 Юрий. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    private var hasCalc = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 0...10:
            if hasCalc {
                resultLabel.text = ""
                hasCalc = false
            }
            resultLabel.text! += (sender.titleLabel?.text)!
            
        case 11...18:
            if hasCalc {
                resultLabel.text = ""
                hasCalc = false
            }
            resultLabel.text! += " "
            resultLabel.text! += (sender.titleLabel?.text!)!
            resultLabel.text! += " "
            
        case 100:
            let result = RPN.Calculate(resultLabel.text!)
            resultLabel.text = String(result)
            hasCalc = true
            
        case 101:
            var str = resultLabel.text!
            
            if str.count == 0 {
                resultLabel.text = ""
            }
            else {
                _ = str.removeLast()
                resultLabel.text = str
            }
        default:
            break
        }
    }
    


}

