//
//  RPN.swift
//  PolishCalculator
//
//  Created by Юрий on 9/27/18.
//  Copyright © 2018 Юрий. All rights reserved.
//

import Foundation

class RPN {
    
    class func Calculate(_ inputString: String) -> Double {
        let str = getExpression(inputString)
        let result = Counting(str)
        return result
    }
    
    private class func getExpression(_ inputString: String) -> String {
        var output = ""
        var operStack: [Character] = []
        let length = inputString.count
        var strInd = 0
        var inputChar: Character = " "
        while strInd < length {
            inputChar = getChar(inputString, strInd)
            if isDelimeter(inputChar) { // проверка на разделитель
                strInd += 1
                continue
            }
            if inputChar >= "0" && inputChar <= "9" { //проверка на число
                while !isDelimeter(inputChar) && !isOperator(inputChar) {
                    output += String(inputChar)
                    strInd += 1
                    if strInd == length {
                        break
                    }
                    inputChar = getChar(inputString, strInd)
                }
                output += " "
                strInd -= 1
                inputChar = getChar(inputString, strInd)
            }
            
            if isOperator(inputChar) { // проверка на оператор
                if inputChar == "(" {
                    operStack.append(inputChar)
                } else if inputChar == ")" {
                    var s = operStack.popLast()!
                    while s != "(" {
                        output += String(s) + " "
                        s = operStack.popLast()!
                    }
                } else {
                    if operStack.count > 0 {
                        while operStack.count > 0 && getPriority(inputChar) < getPriority(operStack.last!)  {
                                let str = operStack.popLast()!
                                output += String(str) + " "
                            }
                    }
                    operStack.append(inputChar)
                }
            }
            strInd += 1
        }
        while operStack.count > 0 {
            output += String(operStack.popLast()!) + " "
        }
        print(output)
        return output
    }
    
    private class func Counting(_ inputString: String) -> Double {
        var result = 0.0
        var temp: [Double] = []
        let length = inputString.count
        var strInd = 0
        var inputChar: Character = " "
        
        while strInd < length {
            inputChar = getChar(inputString, strInd)
            if inputChar >= "0" && inputChar <= "9" {
                var a = ""
                while (!isDelimeter(inputChar) && !isOperator(inputChar)) {
                    a += String(inputChar)
                    strInd += 1
                    inputChar = getChar(inputString, strInd)
                    if strInd == length {
                        break
                    }
                }
                temp.append(Double(a)!)
                strInd -= 1
                inputChar = getChar(inputString, strInd)
            } else if isOperator(inputChar) {
                let a = temp.popLast()!
                let b = temp.popLast()!
                
                switch inputChar {
                case "+": result = b + a
                case "-": result = b - a
                case "*": result = b * a
                case "/": result = b / a
                case "^": result = extent(b, a)
                default: break
                }
                temp.append(result)
            }
            strInd += 1
        }
        return temp.last!
    }
    
    private class func getPriority(_ c: Character) -> Int {
        switch c {
        case "(":
            return 0
        case ")":
            return 1
        case "+":
            return 2
        case "-":
            return 3
        case "*","/":
            return 4
        case "^":
            return 5
        default:
            return 6
        }
    }
    
    private class func isOperator(_ c: Character) -> Bool {
        if ("-+*/^()".firstIndex(of: c) != nil) {
            return true
        }
        return false
    }
    
    private class func isDelimeter(_ c: Character) -> Bool {
        if (" =".firstIndex(of: c) != nil) {
            return true
        }
        return false
    }
    
    private class func getChar(_ inputString: String, _ strInd: Int) -> Character {
        let index = inputString.index(inputString.startIndex, offsetBy: strInd)
        let inputChar = inputString[index]
        return inputChar
    }
    
    private class func extent(_ curNum: Double, _ prevNum: Double) -> Double {
        var resNum = curNum
        var extNum = Int(prevNum)
        while extNum != 1 {
            resNum *= curNum
            extNum -= 1
        }
        return resNum
    }
}
